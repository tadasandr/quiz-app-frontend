<h2>Quiz App front-end</h2>
<br/>
<p>Quiz app I was developing for my portfolio. This is the front-end side. You can find the back-end here <a href="https://gitlab.com/tadasandr/quiz-app-backend">here</a></p>
<p>This application was developed using React.js</p>
<p>Some of the features: </p>
<ul>
<li>Interactive UI</li>
<li>Category selection</li>
<li>Point system</li>
<li>Leaderboards</li>
<li>Time limit</li>
<li>Game flow</li>
<li>Using external APIs. Questions from <a href="https://the-trivia-api.com/">Trivia API</a></li>
<li>HTTP requests</li>
</ul>
<p>Some examples from the app</p>
<img src="https://gitlab.com/tadasandr/quiz-app-frontend/-/raw/main/src/assets/game1.png"/>
<img src="https://gitlab.com/tadasandr/quiz-app-frontend/-/raw/main/src/assets/game2.png"/>
<img src="https://gitlab.com/tadasandr/quiz-app-frontend/-/raw/main/src/assets/game3.png"/>
