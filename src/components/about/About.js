import React from "react";
import { BounceLoader } from "react-spinners";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faStar,
  faClock,
  faStairs,
  faQuestionCircle,
  faFolder,
} from "@fortawesome/free-solid-svg-icons";

import "./About.css";

const About = () => {
  return (
    <div class="table-wrapper-scroll-y my-custom-scrollbar">
      <table className="table table-about">
        <thead>
          <tr>
            <th scope="col">Indicator</th>
            <th scope="col">Meaning</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="col">
              <BounceLoader
                color="crimson"
                size={30}
                aria-label="Loading Spinner"
                data-testid="loader"
              />
            </th>
            <th scope="col">Indicates that the game is ongoing</th>
          </tr>
          <tr>
            <th scope="col">
              <FontAwesomeIcon style={{ color: "gold" }} icon={faStar} />
            </th>
            <th scope="col">Current points value. Points calculated by formula time * difficulty. Multipliers: HARD - 3X, NORMAL - 2X, EASY - 1X.</th>
          </tr>
          <tr>
            <th scope="col">
              <FontAwesomeIcon icon={faClock} />
            </th>
            <th scope="col">Time left for points. Each question gets 45 seconds.</th>
          </tr>
          <tr>
            <th scope="col">
              <FontAwesomeIcon icon={faStairs} />
            </th>
            <th scope="col">Question difficulty. Easy/Normal/Hard.</th>
          </tr>
          <tr>
            <th scope="col">
              <FontAwesomeIcon icon={faQuestionCircle} />
            </th>
            <th scope="col">Current question number out of 10.</th>
          </tr>
          <tr>
            <th scope="col">
              <FontAwesomeIcon icon={faFolder} />
            </th>
            <th scope="col">Current question category.</th>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default About;
