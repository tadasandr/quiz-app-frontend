import React from 'react';

import './Button.css';

const Button = props => {

    const styles = `btn btn-primary btn-lg button-fix ${props.className}`

    return(
        <button disabled={props.disabled} onClick={props.onClick} type={props.type} className={styles}>{props.children}</button>
    )
}

export default Button;