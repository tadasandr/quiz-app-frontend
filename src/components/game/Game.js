import Button from "../UI/Button";
import React, { useEffect, useState, useRef } from "react";
import "./Game.css";
import "../../animations.css";

import { useDispatch, useSelector } from "react-redux";
import {
  incrementQuestionNumber,
  reset,
  startGame,
  getQuestions,
  setGameOver,
  setCategory,
} from "../../store/Store";
import { Link } from "react-router-dom";
import { RingLoader } from "react-spinners";
import Infobox from "../infobox/Infobox";
import FinalScore from "../infobox/FinalScore";
import useQuery from "../../utility/UseQuery";

const Game = (props) => {
  const infoRef = useRef();

  const [nextShown, setNextShown] = useState(false);
  const [isCorrect, setIsCorrect] = useState(false);

  const gameOver = useSelector((state) => state.quiz.gameOver);

  const dispatch = useDispatch();
  const query = useQuery();

  const {
    inGame,
    questionNumber,
    question,
    answers,
    correctAnswer,
    isError,
    isLoading,
    fetched,
    questionsSize,
  } = useSelector((state) => state.quiz);

  useEffect(() => {
    if (!inGame) {
      dispatch(startGame());
      const category = query.get("category");
      dispatch(getQuestions(category));
      if (!category) {
        dispatch(setCategory("random"));
      } else {
        dispatch(setCategory(category));
      }
    }
  }, [inGame]);

  useEffect(() => {
    if (inGame && fetched && questionNumber === questionsSize) {
      dispatch(setGameOver(true));
      console.log("setGameOver() ran");
    }
  }, [questionNumber, inGame]);

  const handleAnswer = (answer) => {
    setNextShown(true);
    infoRef.current.setTimerRunningFalse();
    if (correctAnswer === answer) {
      infoRef.current.calculatePoints();
      setIsCorrect(true);
    }
  };

  const handleNext = () => {
    setNextShown(false);
    setIsCorrect(false);
    infoRef.current.resetTimer();
    infoRef.current.setTimerRunningTrue();
    dispatch(incrementQuestionNumber());
  };

  const handleQuit = () => {
    dispatch(reset());
    console.log("game reset");
  };

  const game = (
    <>
      {isCorrect && nextShown && <h1 className="text-success">Correct!</h1>}
      {!isCorrect && nextShown && (
        <div>
          <h1 className="text-error">
            False! The correct answer is {correctAnswer}
          </h1>
        </div>
      )}
      <div className="slide-in-elliptic-top-fwd">
        <h2>{question && question.question}</h2>
        <div className="buttons-wrapper">
          <Button
            disabled={nextShown}
            type="submit"
            onClick={() => handleAnswer(answers[0])}
          >
            {answers[0]}
          </Button>

          <Button
            disabled={nextShown}
            type="submit"
            onClick={() => handleAnswer(answers[1])}
          >
            {answers[1]}
          </Button>

          <Button
            disabled={nextShown}
            type="submit"
            onClick={() => handleAnswer(answers[2])}
          >
            {answers[2]}
          </Button>

          <Button
            disabled={nextShown}
            type="submit"
            onClick={() => handleAnswer(answers[3])}
          >
            {answers[3]}
          </Button>
        </div>
        {nextShown && (
          <Button onClick={handleNext} className="next__button">
            Next
          </Button>
        )}
      </div>
      <Link to="/leaderboard">
        <Button onClick={handleQuit} className="quit__button">
          Quit
        </Button>
      </Link>
    </>
  );

  return (
    <>
      {!gameOver && (
        <Infobox
          ref={infoRef}
          question={question}
          questionNumber={questionNumber}
          questionsSize={questionsSize}
        />
      )}
      <div className="game">
        {isLoading && (
          <div className="loader">
            <RingLoader
              color="crimson"
              size={150}
              aria-label="Loading Spinner"
              data-testid="loader"
            />
            <span className="loading-text">Creating quiz...</span>
          </div>
        )}
        {!isLoading && !isError && fetched && !gameOver && game}
        {gameOver && <FinalScore quit={handleQuit} />}
      </div>
    </>
  );
};

export default Game;
