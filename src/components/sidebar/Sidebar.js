import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { BounceLoader } from "react-spinners";

import "../sidebar/Sidebar.css";

const SideBar = () => {
  const inGame = useSelector((state) => state.quiz.inGame);
  const category = useSelector((state) => state.quiz.category);

  return (
    <aside className="sidebar-wrapper">
      <div className="sidebar-links">
        {inGame && (
          <NavLink
            className="sidebar-link ingame-indication"
            to={`/training?category=${category}`}
          >
            CURRENT GAME
          </NavLink>
        )}
        <NavLink className="sidebar-link" to="/leaderboard">
          LEADERBOARD
        </NavLink>
        <NavLink className="sidebar-link" to="/categories">
          CATEGORIES
        </NavLink>
        {!inGame && (
          <NavLink className="sidebar-link" to="/random">
            <div className="d-flex flex-row justify-content-center">
              RANDOM GAME
            </div>
          </NavLink>
        )}
        <NavLink className="sidebar-link" to="/about">
          ABOUT
        </NavLink>
      </div>
    </aside>
  );
};

export default SideBar;
