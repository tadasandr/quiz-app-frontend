import React, { useEffect, useRef, useState } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrophy, faRankingStar } from "@fortawesome/free-solid-svg-icons";

import { getCategories } from "../../store/CategoriesSlice";
import { transformCategoryToUrl } from "../../utility/transformRegex";

import "./Leaderboard.css";

import { getStandings } from "../../store/LeaderboardSlice";
import { useDispatch, useSelector } from "react-redux";
import { PulseLoader } from "react-spinners";

const Leaderboard = () => {
  const { standings, isError, isLoading } = useSelector(
    (state) => state.leaderboard
  );

  const categories = useSelector((state) => state.categories.categories);
  const [mounted, setMounted] = useState(false);
  const selectRef = useRef();

  const dispatch = useDispatch();

  useEffect(() => {
    if (categories.length === 0 && !mounted) {
      dispatch(getCategories());
    }
  }, [categories, dispatch]);

  useEffect(() => {
    if (!mounted) {
      dispatch(getStandings("random"));
      setMounted(true);
    }
  }, [standings, dispatch]);

  const onCategoryChange = (e) => {
    dispatch(getStandings(transformCategoryToUrl(e.target.value)));
    console.log(e.target.value);
  };

  const goldTrophy = (
    <FontAwesomeIcon className="gold-trophy" icon={faTrophy} />
  );
  const silverTrophy = (
    <FontAwesomeIcon className="silver-trophy" icon={faTrophy} />
  );
  const bronzeTrophy = (
    <FontAwesomeIcon className="bronze-trophy" icon={faTrophy} />
  );
  const placeHolders = [
    goldTrophy,
    silverTrophy,
    bronzeTrophy,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
  ];

  return (
    <>
      <div>
        <div className="d-flex flex-row">
          <select onChange={(e) => onCategoryChange(e)} className="mb-3">
            {Object.keys(categories).map((o) => (
              <option key={o} value={o}>
                {o}
              </option>
            ))}
            <option value={null} defaultValue>
              Random
            </option>
          </select>
          <PulseLoader
            className="moon-loader"
            color="white"
            size={15}
            loading={isLoading}
          />
        </div>
        <div class="table-wrapper-scroll-y my-custom-scrollbar">
          <table className="table table-bordered mb-0 table-custom">
            <thead>
              <tr>
                <th scope="col">
                  <FontAwesomeIcon icon={faRankingStar} />
                </th>
                <th scope="col">Username</th>
                <th scope="col">Category</th>
                <th scope="col">Points</th>
              </tr>
            </thead>
            <tbody>
              {standings.map((s, index) => (
                <tr key={index + 1}>
                  <th scope="row">{placeHolders[index]}</th>
                  <td>{s.username}</td>
                  <td>{s.category}</td>
                  <td>{s.points}</td>
                </tr>
              ))}
            </tbody>
          </table>
          {standings.length === 0 && (
            <span className="text-white">No records found.</span>
          )}
        </div>
      </div>
    </>
  );
};

export default Leaderboard;
