import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

import { leaderboardapi } from "../../services/leaderboardapi";
import { removeSpaces } from "../../utility/transformRegex";

import "./FinalScore.css";

const FinalScore = (props) => {
  const scoreSelector = useSelector((state) => state.quiz.score);
  const categorySelector = useSelector((state) => state.quiz.category);

  const [username, setUsername] = useState("");
  const [submited, setSubmited] = useState(false);
  const [isError, setError] = useState(true);

  const handleQuit = () => {
    props.quit();
  };

  const handleTyping = (e) => {
    setUsername(e.target.value);
    if (username.length > 1) {
      setError(false);
    } else {
      setError(true);
    }
  };

  console.log("CATEGORY " + categorySelector)

  const handleSubmit = () => {
    if (username.length > 1 && !isError && !submited) {
      setSubmited(true);
      const normalizedCategory = removeSpaces(categorySelector);
      leaderboardapi.postRecord({
        username,
        category: normalizedCategory,
        points: scoreSelector,
      });
    }
  };

  return (
    <div className="leaderboard-wrapper">
      <div className="card">
        <div className="card-body">
          <h5 className="card-title text-center">Game over!</h5>
          <p className="card-text">Your final score is: {scoreSelector}</p>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text" id="basic-addon1">
                @
              </span>
            </div>
            <input
              onChange={(e) => handleTyping(e)}
              type="text"
              className="form-control"
              placeholder="Username"
              aria-label="Username"
              aria-describedby="basic-addon1"
            />
          </div>
          {submited && <span className="text-success">Score submitted!</span>}
          <div className="d-flex justify-content-between">
            <button
              disabled={isError || submited}
              onClick={handleSubmit}
              className="btn btn-primary"
            >
              Submit score
            </button>
            <Link to="/leaderboard">
              <button
                onClick={handleQuit}
                className="btn btn-primary"
                style={{ backgroundColor: "crimson", borderColor: "crimson" }}
              >
                Quit
              </button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FinalScore;
