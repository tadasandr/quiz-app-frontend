import React from "react";
import { useEffect, useState, forwardRef, useImperativeHandle } from "react";
import { setScore, setTime } from "../../store/Store";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar, faClock, faStairs, faQuestionCircle, faFolder } from "@fortawesome/free-solid-svg-icons";

import "./Infobox.css";

const Infobox = forwardRef((props, ref) => {
  const [multiplier, setMultipler] = useState(0);
  const [isTimerRunning, setTimerRunning] = useState(true);
  const { score, time } = useSelector((state) => state.quiz);

  const dispatch = useDispatch();

  useEffect(() => {
    if (props.question) {
      switch (props.question.difficulty) {
        case "easy":
          setMultipler(1);
          break;
        case "medium":
          setMultipler(2);
          break;
        case "hard":
          setMultipler(3);
          break;
      }
    }
  }, [props.question]);

  useEffect(() => {
    if (isTimerRunning && time > 0) {
      const timer = setTimeout(() => dispatch(setTime(time - 1)), 1000);

      return () => clearTimeout(timer);
    }
  }, [time, dispatch, isTimerRunning]);

  useImperativeHandle(ref, () => ({
    resetTimer: () => {
      dispatch(setTime(45));
    },
    setTimerRunningTrue: () => {
      setTimerRunning(true);
    },
    setTimerRunningFalse: () => {
      setTimerRunning(false);
    },
    calculatePoints: () => {
      dispatch(setScore(score + multiplier * time));
    },
  }));

  // console.log("INFOBOX COMPONENT RERENDERED")

  return (
    <div className="infobox-wrapper">
      <span className="question-counter">
        <FontAwesomeIcon icon={faClock} /> {time}
      </span>
      <div className="v1"></div>
      <span className="question-counter">
        <FontAwesomeIcon style={{ color: "gold" }} icon={faStar} /> {score}
      </span>
      <span className="question-counter">
      <FontAwesomeIcon icon={faQuestionCircle} /> {props.questionNumber + 1}/{props.questionsSize}
      </span>
      {props.question && (
        <span className="question-counter">
          <FontAwesomeIcon icon={faStairs} /> {props.question.difficulty}
        </span>
      )}
      {props.question && (
        <span className="question-counter">
          <FontAwesomeIcon icon={faFolder} /> {props.question.category}
        </span>
      )}
    </div>
  );
});

export default Infobox;
