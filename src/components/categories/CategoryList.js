import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCategories } from "../../store/CategoriesSlice";
import CategoryItem from "./CategoryItem";
import { RingLoader } from "react-spinners";

import "./CategoryList.css";

const CategoryList = () => {
  const { categories, isError, isLoading } = useSelector(
    (state) => state.categories
  );
  const inGame = useSelector((state) => state.quiz.inGame);
  const dispatch = useDispatch();

  useEffect(() => {
    if (categories.length === 0) {
      dispatch(getCategories());
    }
  });

  return (
    <>
      {isLoading ? (
        <div className="m-auto loader">
          <RingLoader
            color="crimson"
            size={150}
            aria-label="Loading Spinner"
            data-testid="loader"
          />
          <span className="loading-text">Loading categories...</span>
        </div>
      ) : (
        <div className="container mt-5">
          <h1 className="text-light">
            Educate & train yourself in specific categories!
          </h1>
          {inGame && <span className="ingame-error">You're already in a game!</span>}
          <div className="row">
            {Object.keys(categories).map((c) => (
              <CategoryItem key={c} title={c} disabled={inGame} />
            ))}
          </div>
        </div>
      )}
    </>
  );
};

export default CategoryList;
