import React from "react";
import { Link } from "react-router-dom";
import { transformCategoryToUrl } from "../../utility/transformRegex";

import "./CategoryItem.css";

const CategoryItem = (props) => {

  const styles = props.disabled ? "col mt-5 grow categoryItem inactive" : "col mt-5 grow categoryItem";

  return (
    <Link className={styles} to={`/training?category=${transformCategoryToUrl(props.title)}`}>
      <span>{props.title}</span>
    </Link>
  );
};

export default CategoryItem;
