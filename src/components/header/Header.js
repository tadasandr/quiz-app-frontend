import React from "react";
import { useSelector } from "react-redux";
import { BounceLoader } from "react-spinners";

import './Header.css';

const Header = () => {

    const inGame = useSelector((state) => state.quiz.inGame);

    return(
        <div className="header-wrapper">
            <h1 className="logo">Quiz Game</h1>
            {inGame && (
              <BounceLoader
                color="crimson"
                size={30}
                aria-label="Loading Spinner"
                data-testid="loader"
              />
            )}
        </div>
    )
}

export default Header;