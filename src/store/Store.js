import {
  createSlice,
  configureStore,
  createAsyncThunk,
} from "@reduxjs/toolkit";

import quizapi from "../services/quizapi";
import shuffleArray from "../utility/shuffle";
import categoriesStateSlice from "./CategoriesSlice";
import leaderboardSlice from "./LeaderboardSlice";

const initialState = {
  inGame: false,
  category: null,
  questionNumber: 0,
  questions: [],
  answers: [],
  question: null,
  correctAnswer: "",
  questionsSize: 0,
  isLoading: false,
  isError: false,
  fetched: false,
  score: 0,
  gameOver: false,
  time: 45,
};

export const getQuestions = createAsyncThunk(
  "quiz/random",
  async (category, thunkAPI) => {
    try {
      if(category === null) {
        return await quizapi.fetchRandomQuestions();
      } else {
        console.log("fetching by category")
        return await quizapi.fetchQuestionsByCategory(category);
      }
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const quizStateSlice = createSlice({
  name: "quiz",
  initialState,
  reducers: {
    incrementQuestionNumber: (state) => {
      state.questionNumber += 1;
      if (state.questionNumber < 10) {
        state.question = state.questions[state.questionNumber];
        const answers = [
          state.questions[state.questionNumber].correctAnswer,
          ...state.questions[state.questionNumber].incorrectAnswers,
        ];
        const shuffledArray = shuffleArray(answers);
        state.answers = shuffledArray;
        state.correctAnswer =
          state.questions[state.questionNumber].correctAnswer;
      }
    },
    startGame: (state) => {
      state.inGame = true;
    },
    setScore: (state, action) => {
      state.score = action.payload;
    },
    reset: () => initialState,
    setGameOver: (state, action) => {
      state.gameOver = action.payload;
    },
    setTime: (state, action) => {
      state.time = action.payload;
    },
    setCategory: (state, action) => {
      state.category = action.payload;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getQuestions.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.questions = action.payload;
        state.fetched = true;
        const answers = [
          state.questions[state.questionNumber].correctAnswer,
          ...state.questions[state.questionNumber].incorrectAnswers,
        ];
        const shuffledArray = shuffleArray(answers);
        state.answers = shuffledArray;
        state.question = state.questions[state.questionNumber];
        state.correctAnswer =
          action.payload[state.questionNumber].correctAnswer;
        state.questionsSize = state.questions.length;
      })
      .addCase(getQuestions.rejected, (state) => {
        state.isLoading = false;
        state.isError = true;
        state.questions = [];
      })
      .addCase(getQuestions.pending, (state) => {
        state.isLoading = true;
        state.isError = false;
        state.questions = [];
      })
  },
});

const store = configureStore({
  reducer: {
    quiz: quizStateSlice.reducer,
    categories: categoriesStateSlice.reducer,
    leaderboard: leaderboardSlice.reducer,
  },
});

export const {
  incrementQuestionNumber,
  startGame,
  reset,
  setScore,
  setGameOver,
  setTime,
  setCategory
} = quizStateSlice.actions;

export default store;
