import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import quizapi from "../services/quizapi";

export const getCategories = createAsyncThunk(
  "quiz/categories",
  async (thunkAPI) => {
    try {
      return await quizapi.fetchCategories();
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const initialState = {
    categories: [],
    isError: false,
    isLoading: true,
};

const categoriesStateSlice = createSlice({
  name: "categories",
  initialState,
  reducers: {
    reset: () => initialState
  },
  extraReducers: (builder) => {
    builder
      .addCase(getCategories.fulfilled, (state, action) => {
        state.categories = action.payload;
        state.isLoading = false;
        state.isError = false;
      })
      .addCase(getCategories.rejected, (state) => {
        state.isLoading = false;
        state.isError = true;
      })
      .addCase(getCategories.pending, (state) => {
        state.isLoading = true;
        state.isError = false;
      });
  },
});

export default categoriesStateSlice;