import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { leaderboardapi } from "../services/leaderboardapi";

export const getStandings = createAsyncThunk(
  "quiz/standings",
  async (category, thunkAPI) => {
    try {
      console.log("SLICE REQUEST RAN")
      return await leaderboardapi.getRecords(category);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const initialState = {
    standings: [],
    isError: false,
    isLoading: true
};

const leaderboardSlice = createSlice({
  name: "standings",
  initialState,
  reducers: {
    reset: () => initialState
  },
  extraReducers: (builder) => {
    builder
      .addCase(getStandings.fulfilled, (state, action) => {
        state.standings = action.payload;
        state.isError = false;
        state.isLoading = false;
      })
      .addCase(getStandings.rejected, (state) => {
        state.isError = true;
        state.isLoading = false;
      })
      .addCase(getStandings.pending, (state) => {
        state.isError = false;
        state.isLoading = true;
      });
  },
});

export default leaderboardSlice;