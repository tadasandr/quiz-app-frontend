import axios from "axios";

const API_URL_RANDOM = "https://the-trivia-api.com/api/questions";
const API_URL_CATEGORIES = "https://the-trivia-api.com/api/categories";
const API_URL_QUESTIONS = "https://the-trivia-api.com/api/questions?categories=";

const fetchRandomQuestions = async() => {
    console.log("fetched from API");
    const response = await axios.get(API_URL_RANDOM);
    console.log(response.data);
    return response.data;
}

const fetchCategories = async() => {
    console.log("fetched categories");
    const response = await axios.get(API_URL_CATEGORIES);
    return response.data;
}

const fetchQuestionsByCategory = async(category) => {
    console.log(`${API_URL_QUESTIONS}${category}/limit=10`)
    const response = await axios.get(`${API_URL_QUESTIONS}${category}&limit=10`)
    return response.data;
}

const quizapi = {
    fetchRandomQuestions,
    fetchCategories,
    fetchQuestionsByCategory
};

export default quizapi;