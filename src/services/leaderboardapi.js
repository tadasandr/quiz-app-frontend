import axios from "axios";
// const RECORDS_URL = "http://localhost:5000/api/leaderboards";
const RECORDS_URL = `${window.location.protocol}//${window.location.host}/api/leaderboards`.replace(":3000", ":5000")
console.log("HOST: " + RECORDS_URL)

const getRecords = async (category) => {
  console.log(RECORDS_URL)
  const response = await axios.get(RECORDS_URL, {
    params: {
      category,
    },
  });

  return response.data;
};

const postRecord = async(data) => {
  const response = await axios.post(RECORDS_URL, data);
  return response.data;
}

export const leaderboardapi = {
    getRecords,
    postRecord
} 