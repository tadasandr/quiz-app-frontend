import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import "./App.css";
import About from "./components/about/About";
import CategoryList from "./components/categories/CategoryList";
import Game from "./components/game/Game";
import Header from "./components/header/Header";
import Leaderboard from "./components/leaderboard/Leaderboard";
import SideBar from "./components/sidebar/Sidebar";

function App() {
  return (
    <>
      <header>
        <Header />
      </header>
      <main>
        <SideBar />
        <Switch>
          <Route path="/leaderboard">
            <div className="main-content">
              <Leaderboard />
            </div>
          </Route>
          <Route path="/training">
            <div className="main-content">
              <Game />
            </div>
          </Route>
          <Route path="/random">
            <div className="main-content">
              <Game />
            </div>
          </Route>
          <Route path="/categories">
            <div className="categories-content">
              <CategoryList />
            </div>
          </Route>
          <Route path="/about">
            <div className="main-content">
              <About />
            </div>
          </Route>
        </Switch>
        <Redirect to="leaderboard" />
      </main>
    </>
  );
}

export default App;
