export const transformCategoryToUrl = (category) => {
    const transformedAnd = category.replace("&", "and");
    const transformedWhiteSpace = transformedAnd.replace(/\s/g, "_").toLowerCase();
    return transformedWhiteSpace;
}

export const normalizeUrlToReadable = (category) => {
    const transformedAnd = category.replace("and", "&");
    const transformedWhiteSpace = transformedAnd.replaceAll("_", ' ');
    return transformedWhiteSpace;
}

export const removeSpaces = (category) => {
    const removedSpaces = category.replaceAll(/\s/g, "");
    const removeAnds = removedSpaces.replaceAll("&", "");
    const removeUnderScores = removeAnds.replaceAll("_", "");
    return removeUnderScores;
}